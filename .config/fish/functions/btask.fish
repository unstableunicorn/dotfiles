function btask --description "Run a fish command in background"
    fish -c "$argv" &
end
