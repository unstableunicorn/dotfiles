set nocompatible              " be iMproved, required
filetype off                  " required

function! BuildComposer(info)
    if a:info.status != 'unchanged' || a:info.force
        if has('nvim')
            !cargo build --release
        else
            !cargo build --release --no-default-features --features json-rpc
        endif
    endif
endfunction

" vim plug
call plug#begin('~/.vim/plugged')

Plug 'Valloric/YouCompleteMe'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdcommenter'
Plug 'justinmk/vim-dirvish'
Plug 'junegunn/fzf', {'dir': '~/.vim/plugged/fzf', 'do': './install --all'}
Plug 'dylanaraps/wal.vim'
Plug 'chrisbra/csv.vim'
Plug 'euclio/vim-markdown-composer', { 'do': function('BuildComposer') }
Plug 'dag/vim-fish'

call plug#end()

" enable fugitive with dirvish
autocmd FileType dirvish call fugitive#detect(@%)

filetype plugin indent on    " required

let mapleader=" "

" some movement helpers
" Since using some imaps, remap space space so there is no wait when double
" spacing or n*2*space
inoremap <leader><leader> <leader><leader>

"remap esc
imap jj <ESC>

" cover accidental W and Q's
cnoremap WQ wq
cnoremap Wq wq
cnoremap W w
cnoremap Q q

"quick exit all
inoremap <leader>qa <ESC>:qa<CR>
nnoremap <leader>qa <ESC>:qa<CR>

" Functions 
"Enable and disable mouse use
noremap <f12> :call ToggleMouse() <CR>
function! ToggleMouse()
  if &mouse == 'a'
    set mouse=
    "set nonumber
    echo "Mouse usage disabled"
  else
    set mouse=a
    echo "Mouse usage enabled"
    endif
endfunction

" number toggling
set relativenumber
set number
"add variable to track relative number and set to current value
let g:savedrelativenumber=&relativenumber
" real number toggle
function! ToggleRelNum()
    set relativenumber!
    let g:savedrelativenumber=&relativenumber
endfunction

" turn off all numbers
function! ToggleNumbers()
    if &number
        set nonumber
        set norelativenumber
        echo "Numbers hidden"
    else
        set number
        if g:savedrelativenumber
            set relativenumber
        else
            set norelativenumber
        endif
        echo "Numbers restored"
    endif
endfunction

" toggle relative numbers
nnoremap <leader>rn :call ToggleRelNum() <CR>
" toggle all numbers
nnoremap <leader>nn :call ToggleNumbers() <CR>

" add a yank to register and past from register maps
nmap <leader>y "ry
nmap <leader>Y "rY
nmap <leader>yy "ryy
nmap <leader>P "rP
nmap <leader>p "rp

" automatically arrange csv files
let g:csv_autocmd_arrange = 1

" Mappings
" YCM varialbles
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_path_to_python_interpreter = '/usr/bin/python'
let g:ycm_server_python_interpreter = '/usr/bin/python'
let g:ycm_python_binary_path = 'python2'

" get doc ycm command
nnoremap <leader>gd :YcmCompleter GetDoc<CR>
" jump to definition ycm command
nnoremap <leader>jd :YcmCompleter GoToDefinition<CR>
" other ycm mappings to enable
nnoremap <leader>jc :YcmCompleter GoToDeclaration<CR>
nnoremap <leader>ji :YcmCompleter GoToInclude<CR>
nnoremap <leader>jt :YcmCompleter GoTo<CR>

" sudo the f#$% out of that write
cmap w!! w !sudo tee > /dev/null % 


" easier diff update
nnoremap <leader>du :diffupdate <CR>

" easier shortcut for clearing highlights
nnoremap <leader>nh :nohlsearch <CR>

" easy close buffers
nnoremap <leader>bd :bdelete <CR>

" select all easy
nnoremap <leader>sa ggVG<CR>

" centre searching
:nmap n nzz
:nmap N Nzz
:nmap # #zz
:nmap * *zz

nmap j gj
nmap k gk

" Terminal dependancies:
" cursor settings for urxvt
" 1 is blinky block
" 2 is solid block
" 3 blinky underscore
" 4 solid underscore
" 5 blinky pipe bar
" 6 solid pipe bar
au InsertEnter * silent execute "!echo -en \<esc>[5 q"
au InsertLeave * silent execute "!echo -en \<esc>[2 q"

if &term =~ '^screen'
    " tmux knows the extended mouse mode
set ttymouse=xterm2
endif

" diff settings
set diffopt+=vertical
" scrollbind
"set scrollbind
function! ScrollBind(...)
  " Description: Toggle scrollbind amongst window splits
  " Arguments: 'mode' ( optional ) If not given, toggle scrollbind
  "               = 0 - Disable scrollbind
  "                 1 - Enable  scrollbind
  let l:curr_bufnr = bufnr('%')
  let g:scb_status = ( a:0 > 0 ? a:1 : !exists('g:scb_status') || !g:scb_status )
  if !exists('g:scb_pos') | let g:scb_pos = {} | endif

  let l:loop_cont = 1
  while l:loop_cont
    setl noscb
    if !g:scb_status && has_key( g:scb_pos, bufnr('%') )
      call setpos( '.', g:scb_pos[ bufnr('%') ] )
    endif
    execute "wincmd w"
    let l:loop_cont = !( l:curr_bufnr == bufnr('%') )
  endwhile

  if g:scb_status
    let l:loop_cont = 1
    while l:loop_cont
      let g:scb_pos[ bufnr('%') ] = getpos( '.' )
      normal! gg
      setl scb
      execute "wincmd w"
      let l:loop_cont = !( l:curr_bufnr == bufnr('%') )
    endwhile
  else
    let g:scb_pos = {}
  endif

  if g:scb_status
    echom "Enabling scrollbind"
  else
    echom "Disabling scrollbind"
  endif
endfunction

nmap <leader>sb :call ScrollBind() <CR>

" Other Default Settings
set backspace=indent,eol,start
set tabstop=4
set expandtab
set autoindent
set mouse=r
set cursorline
set spellsuggest=best,10
nnoremap <leader>ts :set spell! <CR>

" set folding params
"first disable scrolling, thought it would be good by default but damn anoying very quickly
set nofoldenable
set foldmethod=indent
set foldlevel=0
set foldnestmax=10
set shiftwidth=4
set softtabstop=4
set visualbell
set confirm
set ignorecase
set smartcase
"set t_Co=256
set hlsearch
set incsearch
set history=200
syntax on

" status line
set laststatus=2
set statusline=
set statusline+=%#warningmsg#
set statusline+=%m
set statusline+=[%n]
set statusline+=%r
set statusline+=%{fugitive#statusline()}
set statusline+=[%{&ff}]
set statusline+=\ %f
"set statusline+=\ [l:%l,c:%c %%%p]
set statusline+=\ %h%=%(%l,%c%V%)\ (%03p%%)



"color and windows
colorscheme luna-term
" colorscheme wal
set term=screen-256color
hi Normal guibg=NONE ctermbg=NONE
" make sure shell is set for compatibility with other shells
set shell=/bin/bash
